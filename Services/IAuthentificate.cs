﻿namespace Llama.Services
{
    public interface IAuthentificate
    {
        bool Login(string username, string password);
        void Restore(string username);
        void Logout(string  username);
        void Register();
    }
}
