﻿using Llama.Data;

namespace Llama.Services
{
    public interface IUserData
    {
        string GetKey(int userId);
        void AddHistory(int userId, int chatId, string line);
        List<(int, string)> GetChats(int UserId);
        List<Chat> GetChat(int UserId, int chatID);
        void DeleteChat(int UserId, int chatID);

    }
}
