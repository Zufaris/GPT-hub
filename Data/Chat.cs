﻿namespace Llama.Data
{
    public class Chat
    {
        public static Dictionary<int,  string> _Face = new Dictionary<int, string>() { { 0, "System" }, { 1, "User" }, { 2, "Assistant" }, { 3, "Pseudo-Assistant" }, { 4, "Pseudo-User" } };
        public DateTime Date {  get; set; } = DateTime.Now;
        public int FaceCode {  get; set; }
        public string Content { get; set; }
        public double Temperature { get; set; }
    }
}
