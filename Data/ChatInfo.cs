﻿namespace Llama.Data
{
    public class ChatInfo: BaseEntity
    {
        int UserId { get; set; }
        public string Name { get; set; } = String.Empty;
        public string Description { get; set; } = String.Empty;
        public DateTime Started { get; set; } = DateTime.Now;
        public DateTime LastChange { get; set; }
        public List<Chat>? ChatHistory { get; set; }

    }
}
