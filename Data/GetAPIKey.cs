﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Llama.Data
{
    public class GetAPIKey
    {
        public static string ReadKey(string filename)
        {
            string line;
            try
            {
                List<string> lines = new List<string>();
                string s = Directory.GetCurrentDirectory() + "\\"+filename;
                StreamReader reader = new StreamReader(s);
                do
                    line = reader.ReadLine();
                while (line == null);
                //{
                //   lines.Add(line);
                //}
                reader.Close();
            }
            catch
            {
                line = "";
            }
                return line;
        }
    }
}
