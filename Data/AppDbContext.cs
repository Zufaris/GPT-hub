﻿using Microsoft.EntityFrameworkCore;

namespace Llama.Data
{
    public class AppDbContext: DbContext 
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ChatInfo> ChatInfos { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) 
        { }
    }
}
