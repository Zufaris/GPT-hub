﻿using Llama.Services;
using System.Xml.Linq;

namespace Llama.Data
{
    public class User: BaseEntity, IUserData
    {
        static int count = 0;
        string Name { get; set; }
        string LastName { get; set; } = String.Empty;
        string Email { get; set; } = String.Empty;
        string Phone { get; set; } = String.Empty;
        HashCode PassHash { get; set; }
        string UserName { get; set; }
        DateTime Registered { get; set; } = DateTime.Today;
        string APIKey { get; set; } = String.Empty;
        int Balanse { get; set; } = 0; // prepaid TokenSets
        long Tokens { get; set; } = 0; // Current Token Set Balanse

        public void AddHistory(int userId, int chatId, string line)
        {
            throw new NotImplementedException();
        }

        public void DeleteChat(int UserId, int chatID)
        {
            throw new NotImplementedException();
        }

        public List<Chat> GetChat(int UserId, int chatID)
        {
            throw new NotImplementedException();
        }

        public List<(int, string)> GetChats(int UserId)
        {
            throw new NotImplementedException();
        }

        public string GetKey(int userId)
        {
            return APIKey;
        }

        public User()
        {
            count++;
            Name = "New_user";
            UserName = $"{Name}_{count}";
            APIKey = GetAPIKey.ReadKey("wwwroot\\UserData\\API-key.txt");
            Console.WriteLine(APIKey==""? "No key" : "KeyOK");
        }
    }
}
